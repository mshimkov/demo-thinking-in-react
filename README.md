## Converting the Thinking in React Demo Application from Class Components to Functional Components
### 1. Change classes to functional components using the function keyword or with arrow functions, and type props as the parameter


<img src="./public/Screen Shot 2020-03-12 at 12.16.49 AM.png" width="700" />

 
#### References I used to help me out: 
* https://medium.com/@olinations/10-steps-to-convert-a-react-class-component-to-a-functional-component-with-hooks-ab198e0fa139
* https://www.digitalocean.com/community/tutorials/five-ways-to-convert-react-class-components-to-functional-components-with-react-hooks
* https://usehooks.com/  {for various other hooks and how to use them. Some of them are really cool, like useTheme}
* https://levelup.gitconnected.com/usetypescript-a-complete-guide-to-react-hooks-and-typescript-db1858d1fb9c