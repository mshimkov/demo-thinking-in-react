import React from 'react';
import ReactDOM from 'react-dom';
import FilterableProductTable from './components/FilterableProductTable/FilterableProductTable' 

ReactDOM.render(
  <FilterableProductTable />,
  document.getElementById('root')
);
