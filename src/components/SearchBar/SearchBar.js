import React from 'react';

export default function SearchBar(props) {
    const memoized = (e) => {
      props.onFilterTextChange(e.target.value);
    }
    
     const memoized2 = (e) => {
      props.onInStockChange(e.target.checked);
    }
      return (
        <form>
          <input
            type="text"
            placeholder="Search..."
            value={props.filterText}
            onChange={memoized}
          />
          <p>
            <input
              type="checkbox"
              checked={props.inStockOnly}
              onChange={memoized2}
            />
            {' '}
            Only show products in stock
          </p>
        </form>
      );
  }