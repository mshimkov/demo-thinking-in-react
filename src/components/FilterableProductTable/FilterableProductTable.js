import React from "react";
import ProductTable from "../ProductTable/ProductTable.js";
import SearchBar from "../SearchBar/SearchBar.js";
import DownloadToPDF from "../DownloadtoPDF/DownloadToPDF";
import CreateNewProduct from "../CreateNewProduct/CreateNewProduct";

let PRODUCTS = [
  {
    id: 1,
    category: "Sporting Goods",
    price: "49.99",
    stocked: true,
    name: "Football"
  },
  {
    id: 2,
    category: "Sporting Goods",
    price: "9.99",
    stocked: true,
    name: "Baseball"
  },
  {
    id: 3,
    category: "Sporting Goods",
    price: "29.99",
    stocked: false,
    name: "Basketball"
  },
  {
    id: 4,
    category: "Electronics",
    price: "99.99",
    stocked: true,
    name: "iPod Touch"
  },
  {
    id: 5,
    category: "Electronics",
    price: "399.99",
    stocked: false,
    name: "iPhone 5"
  },
  {
    id: 6,
    category: "Electronics",
    price: "199.99",
    stocked: true,
    name: "Nexus 7"
  }
];

const FilterableProductTable = () => {
  const [filterText, setFilterText] = React.useState("");
  const [inStockOnly, setInStockOnly] = React.useState(false);
  const [products, setProducts] = React.useState(PRODUCTS);
  const [visible, setVisible] = React.useState(false);
  const [childData, SetChildData] = React.useState();
  const handleFilterTextChange = filterText => {
    setFilterText(() => filterText);
  };

  /**
   *  Add a new product function, which takes in the category, price, name of the product
   *  and whether it is stocked or not
   */
  const addProduct = () => {
    setVisible({ visible: true });

    // setProducts((products) => (
    //   [
    //     ...products,
    //     {
    //       category: category,
    //       price: price,
    //       stocked: stocked,
    //       name: name
    //     }
    //   ]
    // ));
  };

  // function to set to false from CreateNewProduct component
  const handler = e => {
    e.preventDefault();
    setVisible(!visible);
  };

  const callBackFunction = (childProps) => {
    SetChildData(childProps)
  }
  console.log(childData)
  const handleInStockChange = inStockOnly => {
    setInStockOnly(() => inStockOnly);
  };
  return (
    <div>
      <SearchBar
        filterText={filterText}
        inStockOnly={inStockOnly}
        onFilterTextChange={handleFilterTextChange}
        onInStockChange={handleInStockChange}
      />
      <ProductTable
        products={products}
        filterText={filterText}
        inStockOnly={inStockOnly}
      />
      <div>
        <button onClick={() => addProduct()}>Add Product</button>
      </div>
      <DownloadToPDF />
      {visible ? <CreateNewProduct action={handler} parentCallback={callBackFunction} /> : null}
    </div>
  );
};

export default FilterableProductTable;
