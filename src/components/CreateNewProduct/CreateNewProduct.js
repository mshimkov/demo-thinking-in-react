import React, { useState } from "react";
import "./CreateNewProduct.css";
/**
 * Component for creating a new product
 */
export default function CreateNewProduct(props) {
  // create state to hold the add new product
  const [newProduct, setNewProduct] = useState({
    name: "",
    price: "",
    inStock: true,
    category: "Sporting Goods"
  });

  const handleChange = event => {
      const value = event.target.type === "checkbox" ? event.target.checked : event.target.value;
      setNewProduct({
          ...newProduct,
          [event.target.name]: value
      });
  };

  const handleSubmit = (e) => {
      e.preventDefault();
      sendData();
  }

  const sendData = () => {
    props.parentCallback(newProduct);
  }

  return (
    <div className="create-product-div">
      <form onSubmit={handleSubmit}>
        <div className="child-create-product-div">
          <button onClick={props.action} >&times;</button>
        </div>

        <div className="child-create-product-div">
          <label>
            Name
            <input
               name="name"
               value={newProduct.name}
              onChange={handleChange}
            />
          </label>
        </div>

        <div className="child-create-product-div">
          <label>
            Price
            <input 
            type="text"
            name="price"
            value={newProduct.price}
            onChange={handleChange} />
          </label>
        </div>

        <div className="child-create-product-div">
          <label>
            In Stock
            <input 
            type="checkbox"
            name="inStock"
            checked={newProduct.inStock}
            onChange={handleChange} />
          </label>
        </div>

        <div className="child-create-product-div">
          <label>Category</label>
          <select
          name='category'
          value={newProduct.category}
          onChange={handleChange}>
            <option value="Sporting Goods">Sporting Goods</option>
            <option value="Electronics">Electronics</option>
          </select>
        </div>

        <div className="child-create-product-div">
          <button type="submit"> Add Product </button>
        </div>
      </form>
    </div>
  );
}
